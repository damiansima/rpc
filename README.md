
# Relacion de librerias

Este proyecto incluye tres proyectos:

- lista_continua: es un proyecto independiente. proporciona un lista para 
alamcenar datos continuos.

- rpc: Proporcina una API para escribir las funciones cliente - servidor y un 
grupo de funciones para comunicar un proceso servidor con un cliente mediante
tcp ip.

- db_server y db_client: Son un procceso servidor que atiende las peticiones
y una libreria que permite escribir un cliente que realizaria las consultas.



# db_client

Es una libreria que contiene todas las funciones necesarias para consultar
por medio de tcpip al servidor db_server. Las ip y puerto se tienen que pasar por 
un archivo de configuración cuando se inicializa la coneccion rpc con  `rpc_init()`


## Ejemplo con la libreria db_client

El siguiente es un ejemplo: 
    
    rpc = rpc_init( "rpc.cfg", "AYD" );
    rpc_connect(rpc);

    db = db_connect(rpc,"db.sql");  
    db_get_row(db,"select * from hosts");
    
    printf("el valor de host es: %s \n", db_get_column());

    db_free(db);
    rpc_free(rpc);


En este caso, primero se creo la conexión **rpc** con el servidor con `rpc_connect()`
y luego sobre esta **rpc** se inicia la db. Luego hay que liberar la memoria allocada
con `db_free()` y con `rpc_free()`.


# db_server

Es un proceso que corre escuchando un puerto, acepta una conexión, atiende las 
consultas y cuando se cierra encuentra una **brocken pipe** o se cierra el socket
termina.

Tanto `db_server` como `db_client` incorporan las librerías de **rpc** y 
**listacontinua**.




# RPC

Es un conjunto de dos librerías, una cliente y otra servidor que sirven para
crear procesos con *remote procedure call:* llamada a procedimiento remoto.

Con estas librerías se logran una comunicacion *send-reply* entre el cliente - 
servidor, enviando una lista con los argumentos para la función que se ejecutara
en el servidor, el cual a su ves realiza un reply con otra lista de respuesta.

Para que esto funcione correctamente es necesario la **lista continua.**


# lista_continua

Es una lita enlazada con componentes fijos, se pueden editar pero no cambiar
de tamaño.

Una particularidad importante es que todos los bloques que la componen están 
almacenados de forma continua, osea que cuando se deja de usar la lista allocada,
se realiza un `free()` para liberar el espacio utilizado.

Otra particularidad es que es portable, porque contiene una cabezera con los 
elementos relativos a su posición, lo que permite enviar la lista por tcpip.

Por cuestiones de compatibilidad, en la librería **rpc** se podría reemplazar
la *listacontinua* por una lista *xml* o *json* aunque en esos casos es necesario
mas procesamiento.








