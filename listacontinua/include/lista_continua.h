/* 
 * File:   lista_continua.h
 * Author: sima
 *
 * Created on 3 de junio de 2014, 22:17
 */

#ifndef LISTA_CONTINUA_H
#define	LISTA_CONTINUA_H 


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>  

typedef struct{
    int key;
    int nbytes;
    int next;
    int prev;
    char data[];
}LIST_BLOCK; 

typedef int (*__handler_list_gfind) (LIST_BLOCK *);

typedef struct{
    int count_list;
    int block_start;
    int block_end;
    int length;
}LIST_CTR; 

typedef struct{
    LIST_CTR ctr;
    LIST_BLOCK block;
}LIST_t; 

#ifndef assert_msg
    #define assert_msg(A) ((A)? 0 : fprintf(stderr,"%s:%u: assert_msg(%s) fail !\n",__FILE__, __LINE__, #A))
#endif

/**
 * void list_init(LIST_t *list,int length)
 *      inicializa los valores de la lista
 * 
 * LIST_t *list_alloc(int length)
 *      inicializa con malloc y el tamaño de nbytes.
 * 
 * LIST_t *list_resize(LIST_t *list, int length)
 *      hace un realloc de la lista modificando el tamaño de la misma.
 */
void list_init(LIST_t *list,int nbytes);

LIST_t *list_alloc(int nbytes);

LIST_t *list_resize(LIST_t *list, int length);

/**
 * LIST_BLOCK *list_add(LIST_t *list, int nbytes, int key)
 * LIST_BLOCK *list_add_alloc(LIST_t **list, int nbytes, int key)
 * 
 * @return
 *      agrgan un bloque de tamaño nbytes con la lave key y retorna el puntero
 *      del bloque, si no puede hacerlo retorna NULL.
 * 
 */
LIST_BLOCK *list_add(LIST_t *list, int nbytes, int key);

LIST_BLOCK *list_add_alloc(LIST_t **list, int nbytes, int key);

LIST_BLOCK *list_get_block(LIST_t *list, int key); 

LIST_BLOCK *list_first_block(LIST_t *list);

LIST_BLOCK *list_next_block(LIST_t *list, LIST_BLOCK *block);


/**
 * LIST_BLOCK *list_generic_find(LIST_t *list, __handler_list_gfind func)
 * 
 *      Recorre toda la lista llamado a una funcion handler con el argumento
 *      el puntero del bloque. cuando la funcion handlrer retorna TRUE
 *      finalizara el recorrido.
 * 
 *      Es una funcion util para hacer un busqueda de un bloque personalizada.
 * 
 */
LIST_BLOCK *list_generic_find(LIST_t *list, __handler_list_gfind func);

/**
 * LIST_BLOCK *list_pop(LIST_t *list)
 *      Elimina el ultimo bloque agregado.
 */
LIST_BLOCK *list_pop(LIST_t *list);

/**
 * int length_memory(LIST_t *list)
 *      retorna el tamño total de memoria allocada para la lista.
 */
#define length_memory(l) ((l)->ctr.length + sizeof(LIST_CTR))

/**
 * int length_list(LIST_t *list)
 *      retorna el tamño minimo para que toda la lista pueda ser copiada
 *      transmitida, etc.
 */
#define length_list(l) ((l)->ctr.block_end + sizeof(LIST_CTR))

/**
 * int list_is_void(LIST_t *list)
 *      retorna TRUE si la lista esta vacia.
 */
#define list_is_void(l) ( (l==NULL) ? 1 : (((l)->ctr.block_end==0) ? 1 : 0 ))
                                                
#endif	/* LISTA_CONTINUA_H */
