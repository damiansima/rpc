/* 
 * File:   lista_continua.h
 * Author: sima
 *
 * Created on 3 de junio de 2014, 22:17
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>  
#include "../include/lista_continua.h"




LIST_BLOCK *list_get_block(LIST_t *list, int key) 
{
    LIST_BLOCK *block=NULL;
    int i;
    
    if (list->ctr.count_list <= 0)
        return (LIST_BLOCK *)NULL; 
    
    block = (LIST_BLOCK *)((char *)&list->block);  
    if(block->key==key)
        return block;
    
    for(i=1; i < list->ctr.count_list && block->next>=0; i++)
    {
        block = (LIST_BLOCK *)((char *)&list->block + block->next);  
        if(block->key==key)
            return block;
    }
    return (LIST_BLOCK *)NULL;  
}


LIST_BLOCK *list_pop(LIST_t *list)
{
    LIST_BLOCK *block, *block_prev;
    if(list->ctr.count_list==0)
        return (LIST_BLOCK *)NULL;
    list->ctr.block_end = list->ctr.block_start;
    block = (LIST_BLOCK *)((char *)&list->block + list->ctr.block_end);
    list->ctr.block_start = block->prev;
    block_prev =(LIST_BLOCK *)((char *)&list->block + block->prev);  
    block_prev->next =  -1;
    list->ctr.count_list--;
    return block;
}

LIST_BLOCK *list_add(LIST_t *list, int nbytes, int key) 
{
    LIST_BLOCK *block, *block_prev;
    if assert_msg(list!=NULL) return NULL;
    if assert_msg((list->ctr.length - list->ctr.block_end - nbytes - sizeof(LIST_BLOCK)) >0 )
        return (LIST_BLOCK *)NULL;
    
    if (key && assert_msg(list_get_block(list,key)==NULL))
        return (LIST_BLOCK *)NULL;
    
    block = (LIST_BLOCK *)((char *)&list->block + list->ctr.block_end);
    block->key = key;
    block->nbytes = nbytes;
    block->next =-1;
    block->prev = -1;
//    memset(block->data,0,nbytes);
    
    if(list->ctr.count_list > 0)
    {
        block->prev = list->ctr.block_start;
        block_prev =(LIST_BLOCK *)((char *)&list->block + block->prev);  
        block_prev->next =  list->ctr.block_end;
    }
    list->ctr.block_start = list->ctr.block_end;
    list->ctr.block_end = list->ctr.block_start + nbytes + sizeof(LIST_BLOCK);
    list->ctr.count_list++;
    
    return block;
}



LIST_BLOCK *list_first_block(LIST_t *list) 
{
    if assert_msg(list!=NULL) return NULL;
    return (LIST_BLOCK *)((char *)list + sizeof(LIST_BLOCK));  
}

LIST_BLOCK *list_next_block(LIST_t *list, LIST_BLOCK *block) 
{
    if assert_msg(list!=NULL) return NULL;
    if assert_msg(block!=NULL) return NULL;
    if(block->next == -1 )
    {
        return (LIST_BLOCK *)NULL;  
    }
    else 
    {
       return (LIST_BLOCK *)((char *)&list->block + block->next);  
    }
}



void list_init(LIST_t *list,int length)
{
    if assert_msg(list!=NULL) return;
    list->ctr.block_end=0;
    list->ctr.block_start=-1;
    list->ctr.count_list=0;
    list->ctr.length = length - sizeof(LIST_CTR);
    return;
}

LIST_t *list_alloc(int length)
{
    LIST_t *list;
    if assert_msg(length>0) return NULL;
    list = (LIST_t *)malloc( length );
    if assert_msg(list!=NULL) return NULL;
    
    (list)->ctr.block_end=0;
    (list)->ctr.block_start=-1;
    (list)->ctr.count_list=0;
    (list)->ctr.length = length - sizeof(LIST_CTR);
    return list;
}


LIST_t *list_resize(LIST_t *list, int length)
{
    LIST_t *l;
    int res=length%512;
    if assert_msg(list!=NULL) return NULL;
    if(res)
        length += (512-res);
    l = (LIST_t *)realloc( list, length );
    if(l!=NULL)
        l->ctr.length = length - sizeof(LIST_CTR);
    return l;
}



LIST_BLOCK *list_add_alloc(LIST_t **list, int nbytes, int key) 
{
    LIST_t *l;
    if assert_msg(*list!=NULL) return NULL;
    if (((*list)->ctr.length - (*list)->ctr.block_end - nbytes - sizeof(LIST_BLOCK)) <= 0 )
    {
        l=list_resize( *list, (*list)->ctr.length + sizeof(LIST_CTR) + nbytes + sizeof(LIST_BLOCK) +1 );
        if (assert_msg(l!=NULL))
            return (LIST_BLOCK *) -1;
        *list=l;
    }   
    return list_add( *list, nbytes, key);
}



LIST_BLOCK *list_generic_find(LIST_t *list, __handler_list_gfind func) 
{
    LIST_BLOCK *b=NULL;
    int i;
    
    if (list->ctr.count_list <= 0)
        return (LIST_BLOCK *)NULL; 
    
    b = (LIST_BLOCK *)((char *)&list->block);  
    if(func(b))
        return b;
    for(i=0; i < list->ctr.count_list && b->next>=0; i++)
    {
        b = (LIST_BLOCK *)((char *)&list->block + b->next);  
        if(func(b))
            return b;
    }
    return (LIST_BLOCK *)NULL;  
}

