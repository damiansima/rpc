#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 

#include <fcntl.h>
#include <sys/stat.h>

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>

#include "lista_continua.h"
#include "rpc_server.h"


e_rpc_t funcion_test(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    // log_info("funcion_testeo"); 
    // log_info("recibo <<%s>>",list_get_str(lb_rcv,1)); 
    
    list_add_int(lb_reply,1,1233);  
    list_add_float(lb_reply,2,1233.34);  
    list_add_str(lb_reply,3,"hola mundo desde funcion_test");     
    list_add_str(lb_reply,4,"que te pasa calabaza asd  aSD As dAS D0"); 
    return RPC_OK;   
}


e_rpc_t w_fread(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    FILE *fp;
    char *data;
    int length,nr;
    
    length=list_get_int(lb_rcv,1);
    fp=list_get_ptr(lb_rcv,2);
    
    fprintf(stderr,"FILE:%i size:%i\n",(int)fp,length);
    
    data = malloc(length+1);
    
    nr = fread(data,1,length,fp); 
//    fprintf(stderr,"size read:%i data:%s\n",nr,data);
    list_add_int(lb_reply,1,nr); 
    list_add_raw(lb_reply,2,data,nr);
    
    return RPC_OK;   
}

e_rpc_t w_fopen(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    FILE *fp;
    char *filename,*mode;
    
    filename=list_get_str(lb_rcv,1);
    mode=list_get_str(lb_rcv,2);
    fprintf(stderr,"filename:%s mode:%s\n",filename,mode);
    fp = fopen(filename,mode);
    // log_msg("fp: %i",fp);
    
    if(fp==NULL)
        return RPC_FAIL;
    
    list_add_ptr(lb_reply,1,fp);
    
    return RPC_OK;   
}


void do_exit()
{
    puts("chau mundo");
}

void registrar_funciones(void)
{
    registrar_exit_funcion(do_exit);
    registrar_funcion(funcion_test); 
    registrar_funcion(w_fopen); 
    registrar_funcion(w_fread); 
}


unsigned int clen = sizeof(struct sockaddr_in);
struct sockaddr_in client;
    
int main() 
{    
    int sock,csock;
    // logger_file_date("/tmp/rpc_send_rcv","%Y-%m-%d");
    // log_set_file("/tmp/rpcd");
    // log_debug_level(0);
    // log_logger_level(0);     
    
    // sock = bind_tcp("10.210.77.77",7575,10);   
    sock = bind_tcp("0.0.0.0",7576,10);    
    printf("sock:%i\n",sock); 
    if(sock<0)
        return 0;
    
    registrar_funciones();
    
    while(1)
    {
        csock = accept(sock, (struct sockaddr *)&client, &clen);
        // log_info("[RPCD]Conexion desde: %s:%d",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
//         pid=fork();
//         if(pid==0) 
        {
//             close(sock);
            // log_msg("accpt %i !",csock);
			parser_query( csock,csock ); 
        } 
//         close(csock);
        // log_info("[RPCD]Conexion pid:%i %s:%d ",pid,inet_ntoa(client.sin_addr),ntohs(client.sin_port)); 
    } 
    
    return 0;
}
