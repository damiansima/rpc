
#include "../include/rpc_client.h"
#include "lista_continua.h"

#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>    
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <sys/un.h>
#include <string.h>
//#include <photon/PxProto.h>


void rpc_free( RPC_t *rpc )
{
    close(rpc->sock);
    free(rpc);
}


RPC_t *rpc_init( char *file_cfg, char *seccion)
{
    RPC_t *rpc=NULL;
    rpc = (RPC_t *)malloc(sizeof(RPC_t));
    if(!rpc)
        return NULL;
    if( read_cfg_file( rpc, file_cfg, seccion) )
    {
        free(rpc);
        return NULL;
    }
    rpc->sock=-1;
    return rpc;
}

e_rpc_t rpc_connect(RPC_t *rpc) 
{
    rpc->sock = create_tcp_socket( rpc->ip, rpc->port );       
    if( rpc->sock > 0 )
        return RPC_OK;
    rpc->sock = create_tcp_socket( rpc->ip_sec, rpc->port_sec );       
    if( rpc->sock > 0 )
        return RPC_OK; 
    return RPC_NO_CONNECTED;  
}


e_rpc_t rpc_send_msg( RPC_t *rpc, char *call_function, LIST_t *lista_send,LIST_t **lista_reply )
{
    int r;
    rpc_query msg_send;  
    rpc_reply msg_reply;
    int timeseg = 1000;
    
    memset( &msg_send, 0, sizeof(msg_send.header) );  
    strcpy(msg_send.header.function,call_function);
    msg_send.header.length = ( (lista_send==NULL)? 0 : length_list(lista_send));
    memcpy( msg_send.data, lista_send, msg_send.header.length); 
    free(lista_send); 
    fprintf(stderr,"call:<%s> msg_send length:%i\n",msg_send.header.function,msg_send.header.length); 
    fflush(stderr);  
    if(!rpc_is_connect(rpc))
    {
        return RPC_NO_CONNECTED; 
    }
    
    sendall( rpc->sock, (char *)&msg_send, sizeof(msg_send.header) + msg_send.header.length );
    r = readall( rpc->sock, (char *)&msg_reply, sizeof(msg_reply.header), timeseg ); 
    if( r>0 && msg_reply.header.length)
        r = readall( rpc->sock,  (char *)msg_reply.data, msg_reply.header.length, timeseg );
    if(r<=0)
    {
        close(rpc->sock);
        rpc->sock=-1; 
        return RPC_FAIL_CONNECTION; 
    }
    
    if(msg_reply.header.length>0)
    {
        free(*lista_reply);
        *lista_reply = malloc(msg_reply.header.length);
        memcpy( *lista_reply, msg_reply.data, msg_reply.header.length ); 
    }
    return msg_reply.header.return_value;   
} 

int read_cfg_file(RPC_t *rpc, char *file_cfg, char *COD_SECC) 
{
#define isSection(A) (!strcmp(section,A))
#define isString(A) (PxConfigReadString(NULL,A,"",aux,50))
#define LogCONF(A,B) {fprintf(stderr,"%s::%-10s: %s\n",COD_SECC,A,B);fflush(stderr);}

    strcpy(rpc->ip , "0.0.0.0");
    rpc->port = 7576;
    rpc->timeout = 2000; 
    return 0; 
        
//    char *section;
//    int x = -1;
//    char aux[50];
//    
//    
//    fprintf(stderr,"find file cfg: %s\n",file_cfg); 
//    if (PxConfigOpen(file_cfg, PXCONFIG_READ) == NULL) {
//        perror(file_cfg);
//        fprintf(stderr, "Archivo de configuracion no encontrado\n");
//        fflush(stderr);
//        return x;
//    }
//
//    do {
//        section = (char*) PxConfigNextSection();
//        if (isSection(COD_SECC)) 
//        {
//            x=0;
//            if (isString("IP")){
//                LogCONF("IP", aux);
//                strcpy(rpc->ip , aux);
//            }
//            if (isString("PUERTO")){
//                LogCONF("PUERTO", aux);
//                rpc->port = atoi(aux);
//            }
//            if (isString("IP_SEC")){
//                LogCONF("IP_SEC", aux);
//                strcpy(rpc->ip_sec , aux);
//            }
//            if (isString("PUERTO_SEC")){
//                LogCONF("PUERTO_SEC", aux);
//                rpc->port_sec = atoi(aux);
//            }
//            if (isString("TIMEOUT")){
//                LogCONF("TIMEOUT", aux);
//                rpc->timeout = atoi(aux);
//            }
//        }
//    } while (!isSection("FIN"));
//
//    PxConfigClose();
//    return x;
}





