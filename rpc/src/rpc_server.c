#include "../include/rpc_server.h"
#include "lista_continua.h"

static pid_t _pid=0;
#define PID ( _pid ? _pid : (_pid=getpid()) )

rpc_funcion function[128];
__on_exit call_on_exit=NULL;

int num_funciones=0;

void registrar_exit_funcion(__on_exit f) 
{
    call_on_exit = f;
}  

int __registrar_funcion(char *function_name,__function f) 
{
    if(num_funciones==128-1)
        return -1;
    fprintf(stderr,"[PID:%i] funcion registrada: %s\n",PID,function_name);
    function[num_funciones].launch=f; 
    strcpy(function[num_funciones].name, function_name );
    num_funciones++;
    return 0;
}   

e_rpc_t do_function(char *function_name, void *data_rcv, void *data_rply )
{
    int i;
    for(i=0; i<num_funciones;i++ )
    {
        if(!stricmp(function_name,function[i].name))  
        {
            fprintf(stderr,"[PID:%i] encuentra funcion <%s>\n",PID,function[i].name);    
            return function[i].launch( data_rcv, data_rply );  
        }  
    }
    fprintf(stderr,"[PID:%i] NO encuentra funcion <%s>\n",PID,function_name);  
    return RPC_FAIL_NO_FUNCTION; 
}



void loop_parser_query( int fd_write, int fd_read )
{
    rpc_query msg_rcv;
    rpc_reply msg_reply;
    LIST_t *ls_rcv;
    LIST_t *ls_rply;
    int etimedout=3,r=0, nbytes=0, timeseg = 0;
    
    while(1)
    {
        r = readall( fd_read, &msg_rcv, sizeof(msg_rcv.header), timeseg ); 
        if( r>0 && msg_rcv.header.length)
            r = readall( fd_read, msg_rcv.data, msg_rcv.header.length, timeseg );
        if(r<=0)
        {
//            if(errno == ETIMEDOUT && etimedout)
//            {
//                perror("timeout");
//                etimedout--;
//                continue; 
//            }
            fprintf(stderr,"[PID:%i] error al leer los datos errno:%i:%s\n",PID,errno,strerror(errno));
            break;
        }
        etimedout=3;
        if(!strcmp(msg_rcv.header.function,"EXIT"))
            break; 
        fprintf(stderr,"[PID:%i] function:%s length:%i\n",PID,msg_rcv.header.function,msg_rcv.header.length);  
        
        ls_rcv = (LIST_t *)msg_rcv.data;
        ls_rply = (LIST_t *)msg_reply.data;
        list_init(ls_rply,(sizeof(msg_reply)-sizeof(msg_reply.header)));        
        
        msg_reply.header.return_value = do_function( msg_rcv.header.function, ls_rcv, ls_rply );        
        
        msg_reply.header.length = ( list_is_void(ls_rply) ? 0 : length_list(ls_rply));
        nbytes = sizeof(msg_reply.header)+msg_reply.header.length;
        fprintf(stderr,"[PID:%i] reply bytes:%i\n",PID, nbytes );  
        sendall( fd_write, (char *)&msg_reply, nbytes );
        fprintf(stderr,"[PID:%i] parser_query:: done... \n",PID); 
        
    } 
    if(call_on_exit)
        call_on_exit();
    
    fprintf(stderr,"[PID:%i] FINISH... \n",PID);
    close(fd_write);
    close(fd_read);
}
