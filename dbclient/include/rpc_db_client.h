/* 
 * File:   rpc_test.h
 * Author: sima
 *
 * Created on 21 de septiembre de 2014, 21:00
 */

#ifndef RPC_TEST_H
#define	RPC_TEST_H

#include "lista_continua.h"
#include "rpc_client.h"

#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h> 

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <sys/un.h>
#include <string.h>

typedef struct{
    RPC_t *rpc;
    char pool_name[256];
    struct {
        unsigned disconnect_rpc:1;
    }flags;
    void *handler_mysql;
    int n_columns;
    int n_rows;
    LIST_t *column_names;
    LIST_t *column_values;
}db_connection; 
 

/* 
 * funciones de ejemplo
 */
void funcion_test(RPC_t *rpc);
int w_fread(RPC_t *rpc, char *data, int size, FILE *fp);
FILE *w_fopen(RPC_t *rpc, char *filename, char *mode);


/* db_connect()
 *  
 * 
 */
db_connection *db_connect( RPC_t *rpc, char *pool_name );

int db_disconnect( db_connection *db );

int db_free( db_connection *db );

int db_get_rows( db_connection *db, char *query );  

int db_get_next_row( db_connection *db ); 

int db_get_row( db_connection *db, char *query );  

int db_row_count( db_connection *db );
int db_column_count( db_connection *db );

char *db_get_column( db_connection *db, char *column_name );
char *db_get_column_index( db_connection *db, int index ); 

void print_names(db_connection *db);
void print_values(db_connection *db);

#endif	/* RPC_TEST_H */

                             
