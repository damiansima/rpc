#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 

#include "lista_continua.h"
#include "rpc_db_client.h"
 


int main()
{

    RPC_t *rpc;
    char data[1024];
    int nr;
    FILE *fp;
    db_connection *db;
    
    puts("init");

    rpc = rpc_init("../cfg/rpc.cfg","AYD");
    
    rpc_connect(rpc);

    puts("db.sql");
    db = db_connect(rpc,"db.sql");  
    db_get_rows(db,"select * from hosts");
    db_get_next_row(db);
    print_names(db);
    print_values(db); 

    puts("done...");

    rpc_free(rpc);

    return (0);
}
