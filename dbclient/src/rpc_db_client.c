#include "../include/rpc_db_client.h"


void funcion_test(RPC_t *rpc) {
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send = list_alloc(512);
    e_rpc_t ret;
    list_add_str(lr_send, 1, "hola mundo desde cliente");

    ret = rpc_send_msg(rpc, "funcion_test", lr_send, &lr_rply);
    if (ret != RPC_OK) {
        //printf("%s\n",rpc_strerror(ret));
        free(lr_rply);
        return;
    }

    if (list_has_id(lr_rply, 4)) {
        //printf("data %s\n", list_get_str(lr_rply, 4));
    }

    //printf("data %li\n", list_get_int(lr_rply, 1));
    //printf("data %f\n", list_get_float(lr_rply, 2));

    free(lr_rply);
}

int w_fread(RPC_t *rpc, char *data, int size, FILE *fp) {
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send = list_alloc(1024);
    int nr;

    list_add_int(lr_send, 1, size);
    list_add_ptr(lr_send, 2, fp);

    if( rpc_send_msg(rpc, "w_fread", lr_send, &lr_rply) != RPC_OK )
    {   
        free(lr_rply); 
        return 0;
    }
    
    nr = list_get_int(lr_rply, 1);
    memcpy(data, list_get_str(lr_rply, 2), nr);
    free(lr_rply);
    return nr;
}

FILE *w_fopen(RPC_t *rpc, char *filename, char *mode) {
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send = list_alloc(1024);
    FILE *fp = NULL;
    e_rpc_t ret;
    list_add_str(lr_send, 1, filename);
    list_add_str(lr_send, 2, mode);

    ret = rpc_send_msg(rpc, "w_fopen", lr_send, &lr_rply);
    if (ret != RPC_OK) { 
        //printf("%s\n",rpc_strerror(ret));
        free(lr_rply); 
        return NULL;
    }

    if (list_has_id(lr_rply, 1)) {
        fp = list_get_ptr(lr_rply, 1); 
        //printf("fp %i\n", (int)fp);
    }
    free(lr_rply);  
    return fp;
}


db_connection *db_connect( RPC_t *rpc, char *pool_name )
{
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send;
    db_connection *db=NULL;
    int disconnect_rpc=0;
    RPC_t *rpc_db;
    
    if( rpc==NULL )
    {
        fprintf(stderr,"Debe pasar un rpc inicializado ! \n");
        return NULL;
    }
    if( rpc->sock<0 )
    {
        fprintf(stderr,"Se usara un rpc interno ! \n");
        rpc_db = malloc(sizeof(RPC_t));
        memcpy(rpc_db,rpc,sizeof(RPC_t));  
        if(rpc_connect(rpc_db)!=RPC_OK)
        {
            fprintf(stderr,"**** NO se puede conectar! \n");
            rpc_free(rpc_db);
            return NULL;
        }
        disconnect_rpc = 1; 
    }
    else
    {
        disconnect_rpc=0;
        rpc_db=rpc;
    }
    fprintf(stderr,"**** Ok, rpc conectado! \n");
        
    lr_send = list_alloc(1024);
    fprintf(stderr,"pool_name:%s\n",pool_name);
    list_add_str(lr_send, 1, pool_name);
    if( rpc_send_msg(rpc_db, "db_connect", lr_send, &lr_rply) != RPC_OK )  
        return NULL;
    
    if (list_has_id(lr_rply, 1)) 
    {
        // inicializa la conneccion
        db=malloc(sizeof(db_connection));
        strcpy(db->pool_name,pool_name);
        db->rpc=rpc_db;   
        db->flags.disconnect_rpc = disconnect_rpc; 
        db->column_names=NULL;
        db->column_values=NULL;
        db->n_columns=-1;
        db->n_rows=-1;
        db->handler_mysql = list_get_ptr(lr_rply, 1);
        //fprintf(stderr,"db->handler_mysql %i\n", (int)db->handler_mysql);
    }
    free(lr_rply);    
    return db;     
}

int db_disconnect( db_connection *db ) 
{
	LIST_t *lr_rply = NULL;
    LIST_t *lr_send;
    
    if( db==NULL || db->rpc==NULL || db->handler_mysql==NULL )
        return RPC_FAIL;
    
    lr_send = list_alloc(1024);
    list_add_ptr(lr_send, 1,db->handler_mysql );
    if( rpc_send_msg( db->rpc, "db_disconnect", lr_send, &lr_rply) != RPC_OK ) 
        return RPC_FAIL; 
    db->handler_mysql = NULL;
    free(lr_rply); 
    return RPC_OK;    
}



int db_execute( db_connection *db, char *sql ) 
{
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send;
    int result;
    
    if(strlen(sql)<10 || db==NULL || db->rpc==NULL )
        return RPC_FAIL;
     
    lr_send = list_alloc(10240);
    list_add_ptr(lr_send, 1,db->handler_mysql );
    list_add_str(lr_send, 2,sql );
    if( rpc_send_msg( db->rpc, "db_execute", lr_send, &lr_rply) != RPC_OK ) 
        return RPC_FAIL;   
    
    result = list_get_int(lr_rply, 1);
    free(lr_rply); 
    return result;   
}

int db_get_rows( db_connection *db, char query[1024] ) 
{
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send; 
    LIST_t *l_column_name;
    
    if(strlen(query)<10 || db==NULL || db->rpc==NULL )
        return RPC_FAIL;
     
    lr_send = list_alloc(10240);
    list_add_ptr(lr_send, 1,db->handler_mysql );
    list_add_str(lr_send, 2,query );
    //fprintf(stderr,"db_get_row::query::<<%s>>\n",query); 
    if( rpc_send_msg( db->rpc, "db_get_rows", lr_send, &lr_rply) != RPC_OK ) 
        return RPC_FAIL; 
    
    if(db->column_names)
      free(db->column_names);
    if(db->column_values)
      free(db->column_values);
    db->n_columns = list_get_int(lr_rply, 1);
    db->n_rows = list_get_int(lr_rply, 2);
    l_column_name = list_get_raw(lr_rply,3);
    db->column_names = malloc(length_list(l_column_name)+10); 
    memcpy(db->column_names,l_column_name,length_list(l_column_name));
    free(lr_rply); 
    return RPC_OK;   
}                                    

int db_get_next_row( db_connection *db ) 
{
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send;
    LIST_t *l_column_values;
    
    if( db==NULL || db->rpc==NULL )
        return RPC_FAIL;
     
    lr_send = list_alloc(1024);
    // fprintf(stderr,"db->handler_mysql %i\n", (int)db->handler_mysql);
    list_add_ptr(lr_send, 1,db->handler_mysql );
    if( rpc_send_msg( db->rpc, "db_get_next_row", lr_send, &lr_rply) != RPC_OK ) 
        return RPC_FAIL; 
    
    if(db->column_values)
      free(db->column_values);  
    l_column_values = list_get_raw(lr_rply,1);
    db->column_values = malloc(length_list(l_column_values)+10);
    memcpy(db->column_values,l_column_values,length_list(l_column_values));
    free(lr_rply); 
    return RPC_OK;    
}

int db_get_row( db_connection *db, char *query ) 
{
    LIST_t *lr_rply = NULL;
    LIST_t *lr_send;
    LIST_t *l_column_name;
    LIST_t *l_column_values;
    
    if(strlen(query)<10 || db==NULL || db->rpc==NULL )
        return RPC_FAIL; 
    
    lr_send = list_alloc(10240);
    list_add_ptr(lr_send, 1,db->handler_mysql );
    list_add_str(lr_send, 2,query );
    if( rpc_send_msg( db->rpc, "db_get_row", lr_send, &lr_rply) != RPC_OK ) 
        return RPC_FAIL; 
    
    if(db->column_names)
      free(db->column_names);
    if(db->column_values)
      free(db->column_values);
    
    db->n_columns = list_get_int(lr_rply, 1);
    db->n_rows = list_get_int(lr_rply, 2);
    l_column_name = list_get_raw(lr_rply,3);
    db->column_names = malloc(length_list(l_column_name)+10);
    memcpy(db->column_names,l_column_name,length_list(l_column_name));
    
    l_column_values = list_get_raw(lr_rply,4);
    db->column_values = malloc(length_list(l_column_values)+10);
    memcpy(db->column_values,l_column_values,length_list(l_column_values));
    
    free(lr_rply);    
    return RPC_OK;   
}

void print_names(db_connection *db)
{
    int i=1; 
    LIST_BLOCK *block;
    if( db==NULL || db->rpc==NULL )
        return ;
    block = list_first_block(db->column_names);
    while(block)
    {
        printf("columna %i :%s\n",i++,block->data);
        block = list_next_block(db->column_names,block);
    } 
}

void print_values(db_connection *db)
{
    LIST_BLOCK *block;
    int i=1;
    if( db==NULL || db->rpc==NULL )
        return ;
    block = list_first_block(db->column_names);
    while(block)
    {
        if(list_has_id(db->column_values, i))
            printf("%4i %30s:%s\n",i,block->data,list_get_str(db->column_values,i));
        //printf("columna %i :%s\n",i,block->data); 
        block = list_next_block(db->column_names,block); 
        i++;
    } 
}


char *db_get_column( db_connection *db, char *column_name )
{
    LIST_BLOCK *block;
    int i=1;
    if( db==NULL || db->rpc==NULL )
        return NULL;
    block = list_first_block(db->column_names);
    while(block)
    {
        if( stricmp(block->data,column_name)==0 )
        {
            if ( list_has_id(db->column_values, i) )
            {
                return list_get_str(db->column_values,i);
            } 
            else 
            {
                return NULL;   
            }
        }
        block = list_next_block(db->column_names,block);
        i++;
    }
    return NULL; 
}

char *db_get_column_index( db_connection *db, int index ) 
{
    if( db==NULL || db->rpc==NULL )
        return NULL;
  
    if( list_has_id(db->column_values, index) )
        return list_get_str( db->column_values, index );
    return NULL; 
}


int db_row_count( db_connection *db )
{
    if( db==NULL || db->rpc==NULL )
        return -1;    
    return ( db->n_rows );
} 

int db_column_count( db_connection *db )
{
    if( db==NULL || db->rpc==NULL )
        return -1;    
    return ( db->n_columns );
} 


int db_free( db_connection *db ) 
{   
    if( db==NULL || db->rpc==NULL )
        return RPC_FAIL;
    if(db->column_names)
      free(db->column_names);
    if(db->column_values)
      free(db->column_values);
    db_disconnect( db );
    if(db->flags.disconnect_rpc)
    {
        //fprintf(stderr,"db_free rpc\n"); 
        rpc_free(db->rpc);
    }
    db->rpc = NULL;           
    db->handler_mysql = NULL;
    free(db);
    return RPC_OK;     
}

