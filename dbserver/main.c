// #############################################################################
//
// Fecha     : $Date: 2014-01-02 13:35:54 +0000 (Thu, 02 Jan 2014) $
// Revision  : $Revision: 3785 $
// Autor     : $Author: ternium\c.jobla $
// URL       : $URL: http://terarautsvn01/n2/Aceracion/Horno_de_Vacio_1/trunk/usr/local/RH/src/procesos/RH_fases_horno/general.c $
//
// Company   : TERNIUM S.A.
//
// $Id: general.c 3785 2014-01-02 13:35:54Z ternium\c.jobla $
//
// #############################################################################


// Include estandar
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>
#include <stdarg.h>


#include <sys/types.h> 

#include <fcntl.h>
#include <sys/stat.h>

#include <time.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>

char DEBUG_FILE[256]={"/tmp/rpc_server"};

#include "lista_continua.h"
#include "rpc_server.h"


// GLOBAL


e_rpc_t funcion_test(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    // log_info("funcion_testeo"); 
    // log_info("recibo <<%s>>",list_get_str(lb_rcv,1)); 
    
    list_add_int(lb_reply,1,1233);  
    list_add_float(lb_reply,2,1233.34);  
    list_add_str(lb_reply,3,"hola mundo desde funcion_test");     
    list_add_str(lb_reply,4,"que te pasa calabaza asd  aSD As dAS D0"); 
    return RPC_OK;   
}


e_rpc_t w_fread(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    FILE *fp;
    char *data;
    int length,nr;
    
    length = list_get_int(lb_rcv,1);
    fp = (FILE *)list_get_ptr(lb_rcv,2);
    
    fprintf(stderr,"FILE:%i size:%i\n",(int)fp,length);
    
    data = malloc(length+1);
    
    nr = fread(data,1,length,fp); 
//    fprintf(stderr,"size read:%i data:%s\n",nr,data);
    list_add_int(lb_reply,1,nr); 
    list_add_raw(lb_reply,2,data,nr);
    
    return RPC_OK;   
}

e_rpc_t w_fopen(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    FILE *fp;
    char *filename,*mode;
    
    filename=list_get_str(lb_rcv,1);
    mode=list_get_str(lb_rcv,2);
    fprintf(stderr,"filename:%s mode:%s\n",filename,mode);
    fp = fopen(filename,mode);
    // log_msg("fp: %i",fp);
    
    if(fp==NULL)
        return RPC_FAIL;
    
    list_add_ptr(lb_reply,1,fp);
    
    return RPC_OK;   
}


e_rpc_t db_connect(LIST_t* lb_rcv, LIST_t* lb_reply)
{
	int res;
	handler *db_han=NULL;
    char *pool_name;
	pool_name = list_get_str(lb_rcv,1);
	
    db_han = new_dbhandler();
    
    res = read_pool (db_han, NULL, pool_name);
    if (res != SUCCEED)           
    {    
		printf_color(PRINT_TEXT_YELLOW,"--- Funcion %s - Error al leer POLL [%s]\n", __FUNCTION__,pool_name);
        return RPC_FAIL; 
    }
    
    res = connect_to_DB(db_han);
    if (res != SUCCEED ) 
    {
        printf_color(PRINT_TEXT_YELLOW,"Funcion %s - Error [%d] de conexion\n", __FUNCTION__,res);
        return RPC_FAIL;
    }
    list_add_ptr(lb_reply,1,db_han);
    return RPC_OK;    
}


e_rpc_t db_disconnect(LIST_t* lb_rcv, LIST_t* lb_reply)
{
	handler *dbh;
	
	dbh = (handler *)list_get_ptr(lb_rcv,1);
	
	if(dbh)
	{
		printf_color(PRINT_TEXT_YELLOW,"se libera el db_han, %i\n",(int)dbh); 
		disconnect_from_DB(dbh);
		free_dbhandler(dbh); 	
		fflush(stdout); 
    } 
	
    return RPC_OK;    
}


e_rpc_t db_execute(LIST_t* lb_rcv, LIST_t* lb_reply)
{
	int res;
	char *sql;
	handler *dbh;
	
	dbh = (handler *)list_get_ptr(lb_rcv,1);
	sql = list_get_str(lb_rcv,2);
	
	if((res = execute_query( dbh, sql )) != SUCCEED )
	{
		printf ("Funcion %s (%d): executing: <%s> \n- Error <%d>\n", __FUNCTION__, __LINE__, sql, res);
		fflush(stdout);
	}
			
	list_add_int( lb_reply, 1, res ); 
	return RPC_OK; 
}



e_rpc_t db_get_rows(LIST_t* lb_rcv, LIST_t* lb_reply)
{
	int res;
	char *query;
	handler *dbh;
	unsigned int ncolumn,nrow,i;
	char **column_name;
	LIST_t *column_names = list_alloc(2048);
	
	dbh = (handler *)list_get_ptr(lb_rcv,1);
	query = list_get_str(lb_rcv,2);
	printf_color(PRINT_TEXT_YELLOW,"query: %s\n", query ); 
	
	if((res = execute_query( dbh, query )) != SUCCEED )
	{
		printf ("Funcion %s (%d): executing: <%s> \n- Error <%d>\n", __FUNCTION__, __LINE__, query, res);
		fflush(stdout);		
		free(column_names);
		return RPC_FAIL;
	}
			
	//RECUPERO DATOS
	res = get_results( dbh ); 
	if( res == SUCCEED )
	{
		
		printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
		ncolumn = get_column_count(dbh);
		nrow = get_row_count(dbh);
		column_name = get_column_names(dbh);
		for(i=0;i<ncolumn;i++)
		{
			list_add_str( column_names, i+1,column_name[i]);
			// printf ("columna %i:: %s \n",i, column_name[i] );
			// fflush(stdout);
		}
	
		list_add_int( lb_reply, 1, ncolumn );
		list_add_int( lb_reply, 2, nrow);		
		list_add_raw( lb_reply, 3, column_names, length_list(column_names) ); 
		free(column_names);
		return RPC_OK;
	}
	free(column_names);
	return RPC_FAIL;
}


e_rpc_t db_get_next_row(LIST_t* lb_rcv, LIST_t* lb_reply)
{
	handler *dbh;
	unsigned int ncolumn,i;
	char **column_name;
	db_string_row row;
	LIST_t *column_values = list_alloc(20480);
	
	dbh = (handler *)list_get_ptr(lb_rcv,1);	
	
	row = get_next_row_as_str (dbh);
	if (row != NULL)
	{
		// int get_row_count(handler *db_con);
		// int *get_column_sizes(handler *db_con);
		printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
		ncolumn = get_column_count(dbh);
		column_name = get_column_names(dbh);
		for(i=0;i<ncolumn;i++)
		{
			printf ("%5i:: %-35s=%s\n",i, column_name[i],(row[i] ? row[i] : "NULL") );
			fflush(stdout);
			if(row[i])
			{
				list_add_str( column_values, i+1,row[i]);
			}
		}
		list_add_raw( lb_reply, 1, column_values, length_list(column_values) ); 
		free(column_values);
		return RPC_OK;  
	}
	
	printf ("Funcion %s (%d): no hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
	free(column_values);
	return RPC_FAIL;
}



e_rpc_t db_get_row(LIST_t* lb_rcv, LIST_t* lb_reply)
{
	int res;
	char *query;
	handler *dbh;
	unsigned int ncolumn,nrow,i;
	char **column_name;
	db_string_row row;
	LIST_t *column_names = list_alloc(2048);
	LIST_t *column_values = list_alloc(20480);
	
	dbh = (handler *)list_get_ptr(lb_rcv,1);
	query = list_get_str(lb_rcv,2);
	printf_color(PRINT_TEXT_YELLOW,"query: %s\n", query ); 
	
	if((res = execute_query( dbh, query )) != SUCCEED )
	{
		printf ("Funcion %s (%d): executing: <%s> \n- Error <%d>\n", __FUNCTION__, __LINE__, query, res);
		fflush(stdout);
		free(column_names);
		free(column_values);
		clear_result_set(dbh); 
		return RPC_FAIL;
	}
			
	res = get_results( dbh );  
	if( res == SUCCEED )
	{
		
		printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
		ncolumn = get_column_count(dbh);
		nrow = get_row_count(dbh);
		column_name = get_column_names(dbh);
		for(i=0;i<ncolumn;i++)
		{
			list_add_str( column_names, i+1,column_name[i]);
		}	
		
		row = get_next_row_as_str (dbh);
		if (row != NULL)
		{
			for(i=0;i<ncolumn;i++)
			{
				if(row[i])
				{
					list_add_str( column_values, i+1,row[i]);
				}
			}
			list_add_int( lb_reply, 1, ncolumn );
			list_add_int( lb_reply, 2, nrow);		
			list_add_raw( lb_reply, 3, column_names, length_list(column_names) ); 
			list_add_raw( lb_reply, 4, column_values, length_list(column_values) ); 
			free(column_names);
			free(column_values);
			clear_result_set(dbh);
			return RPC_OK;
		}
	}
	free(column_names);
	free(column_values);
	clear_result_set(dbh);
	return RPC_FAIL;
}



void do_exit()
{
	puts("do_exit()");
	// l_disconnectDB(db_han);
}

void registrar_funciones(void)
{
    registrar_exit_funcion(do_exit);
    registrar_funcion(funcion_test); 
    registrar_funcion(w_fopen); 
    registrar_funcion(w_fread); 
    registrar_funcion(db_connect); 
    registrar_funcion(db_disconnect); 
    registrar_funcion(db_execute);  
    registrar_funcion(db_get_row);  
    registrar_funcion(db_get_rows);   
    registrar_funcion(db_get_next_row);    
}

/*
handler *w_ConnectDB(char *pool_name)
{ 
	handler *db_han;
	
	int res = SUCCEED;
    

    db_han = new_dbhandler();
    
    res = read_pool (db_han, NULL, pool_name);
    if (res != SUCCEED)           
    {    
		printf_color(PRINT_TEXT_YELLOW,"--- Funcion %s - Error al leer POLL [%s]", __FUNCTION__,pool_name);
        Iv_Debugging(DEBUG_FILE, "Funcion %s - Error al leer POLL [%s]", __FUNCTION__,pool_name);
        return NULL; 
    }
    
    res = connect_to_DB(db_han);
    if (res != SUCCEED ) 
    {
        printf_color(PRINT_TEXT_YELLOW,"Funcion %s - Error [%d] de conexion\n", __FUNCTION__,res);
        Iv_Debugging(DEBUG_FILE, "Funcion %s - Error [%d] de conexion\n", __FUNCTION__,res);
        return NULL;
    }
    return db_han;   
}


int w_SelectDB( handler *db_han)
{
	int result=0;
	int res;
	char query[2048];
	unsigned int ncolumn,i;
	db_string_row row;
	char **column_name;
	
	strcpy(query, "SELECT * FROM pp_entrada ");
	if ((result = execute_query (db_han, query)) != SUCCEED )
	{
		printf ("Funcion %s (%d): executing: <%s> \n- Error <%d>\n", __FUNCTION__, __LINE__, query, result);
		fflush(stdout);
	}
			
	//RECUPERO DATOS
	res = get_results( db_han ); 
	if( res == SUCCEED )
	{
		// row = get_next_row_as_str (db_han);
		// if (row != NULL)
		// {
			// int get_row_count(handler *db_con);
			// int *get_column_sizes(handler *db_con);
			printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
			ncolumn = get_column_count(db_han);
			column_name = get_column_names(db_han);
			for(i=0;i<ncolumn;i++)
			{
				printf ("columna %i:: %s \n",i, column_name[i] );
				fflush(stdout);
			}
		// }
		// else
		// {
			// printf ("Funcion %s (%d): no hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
		// }
	}
	return RPC_OK;
}
	
	
int w_NextRowDB( handler *db_han)
{
	int result=0;
	int res;
	char query[2048];
	unsigned int ncolumn,i;
	db_string_row row;
	char **column_name;
	
	row = get_next_row_as_str (db_han);
	if (row != NULL)
	{
		// int get_row_count(handler *db_con);
		// int *get_column_sizes(handler *db_con);
		printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
		ncolumn = get_column_count(db_han);
		column_name = get_column_names(db_han);
		for(i=0;i<ncolumn;i++)
		{
			printf ("%5i:: %-35s=%s\n",i, column_name[i],(row[i] ? row[i] : "NULL") );
			fflush(stdout);
		}
	}
	else
	{
		printf ("Funcion %s (%d): no hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
	}
	return RPC_OK;
}
	
*/    


	
int main() 
{    
	unsigned int clen = sizeof(struct sockaddr_in);
	struct sockaddr_in client;
    int sock,csock,pid=1111;
	
	//~ IvLog_Init();
	//~ Iv_Log("pepe",1,"pepe2"); 
//~ 
	//~ #define log_info(fmt, args...) IvLog_Save("AYD","rpc_dbserver",Iv_LOG_INFO,fmt,##args)
	//~ #define log_error(fmt, args...) IvLog_Save("AYD","rpc_dbserver",Iv_LOG_ERROR,fmt,##args)
	//~ log_info("yea men");
	//~ log_error("yea men");
	
#define tamanio(t) printf("%-13s=%i\n",#t,sizeof(t))
	
	tamanio(void *);
	tamanio(long int);
	tamanio(int);
	tamanio(char *);
	tamanio(char);
	tamanio(FILE*);
	tamanio(FILE);
	tamanio(time_t); 


	// handler *hc;
	// hc = w_ConnectDB("PISO_PLANTA");
	// w_SelectDB(hc); 
	// w_NextRowDB(hc);
	// l_disconnectDB(hc);
	// return 0;
	
    // logger_file_date("/tmp/rpc_send_rcv","%Y-%m-%d");
    // log_set_file("/tmp/rpcd");
    // log_debug_level(0);
    // log_logger_level(0);     
    
    sock = bind_tcp("10.210.77.77",7575,10);   
    // sock = bind_tcp2("172.17.25.19",7576,10);    
    printf("sock:%i\n",sock); 
    if(sock<0)
        return 0;
    
    registrar_funciones();
    
    while(1)
    {
        csock = accept(sock, (struct sockaddr *)&client, &clen);
        // log_info("[RPCD]Conexion desde: %s:%d",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
        pid=fork();
        if(pid==0) 
        {
//             close(sock);
            // log_msg("accpt %i !",csock);
			parser_query( csock,csock ); 
			exit(0); 
        } 
        close(csock);
        // log_info("[RPCD]Conexion pid:%i %s:%d ",pid,inet_ntoa(client.sin_addr),ntohs(client.sin_port)); 
    } 
    
    return 0;
}
	
	
	
