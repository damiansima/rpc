#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 

#include <fcntl.h>
#include <sys/stat.h>

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <string.h>

#include "lista_continua.h"
#include "rpc_server.h"

#include <sys/stat.h>
#include <errno.h>
#include <sys/types.h>
#include <sqlite3.h>

typedef struct {
    sqlite3 *db;
    sqlite3_stmt *stmt; 
}handler;

handler *new_handler()
{
    handler *h=malloc(sizeof(handler));
    return h;
}

void free_handler(handler *dbh)
{
    sqlite3_close(dbh->db);
    sqlite3_finalize(dbh->stmt);
    free(dbh);
}


#define logger(fmt, args...) {fprintf(stderr,fmt,##args);puts("");}



e_rpc_t funcion_test(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    // log_info("funcion_testeo"); 
    // log_info("recibo <<%s>>",list_get_str(lb_rcv,1)); 
    
    list_add_int(lb_reply,1,1233);  
    list_add_float(lb_reply,2,1233.34);  
    list_add_str(lb_reply,3,"hola mundo desde funcion_test");     
    list_add_str(lb_reply,4,"que te pasa calabaza asd  aSD As dAS D0"); 
    return RPC_OK;   
}


e_rpc_t w_fread(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    FILE *fp;
    char *data;
    int length,nr;
    
    length=list_get_int(lb_rcv,1);
    fp=list_get_ptr(lb_rcv,2);
    
    fprintf(stderr,"FILE:%i size:%i\n",(int)fp,length);
    
    data = malloc(length+1);
    
    nr = fread(data,1,length,fp); 
//    fprintf(stderr,"size read:%i data:%s\n",nr,data);
    list_add_int(lb_reply,1,nr); 
    list_add_raw(lb_reply,2,data,nr);
    
    return RPC_OK;   
}

e_rpc_t w_fopen(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    FILE *fp;
    char *filename,*mode;
    
    filename=list_get_str(lb_rcv,1);
    mode=list_get_str(lb_rcv,2);
    fprintf(stderr,"filename:%s mode:%s\n",filename,mode);
    fp = fopen(filename,mode);
    // log_msg("fp: %i",fp);
    
    if(fp==NULL)
        return RPC_FAIL;
    
    list_add_ptr(lb_reply,1,fp);
    
    return RPC_OK;   
}


e_rpc_t db_connect(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    int res;
    handler *dbh=NULL;
    char *pool_name;
    pool_name = list_get_str(lb_rcv,1);
	
    dbh = new_handler();
    
    logger("sqlite3_open(%s)",pool_name);
    res = sqlite3_open(pool_name, &dbh->db );
    if (res != RPC_OK)           
    {    
        logger("--- Funcion %s - Error al leer POLL [%s]\n", __FUNCTION__,pool_name);
        return RPC_FAIL; 
    }
    
    list_add_ptr(lb_reply,1,dbh);
    return RPC_OK;    
}


e_rpc_t db_disconnect(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    handler *dbh;
    dbh = (handler *)list_get_ptr(lb_rcv,1);
    if(dbh)
    {
        logger("se libera el db_han, %i\n",(int)dbh);
        free_handler(dbh);
    } 
    return RPC_OK;    
}


e_rpc_t db_get_rows(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    int res;
    char *query;
    handler *dbh;
    unsigned int ncolumn,nrow,i;
    LIST_t *column_names = list_alloc(2048);

    dbh = (handler *)list_get_ptr(lb_rcv,1);
    query = list_get_str(lb_rcv,2);
    logger("query: %s", query ); 

    if( (res = sqlite3_prepare_v2(dbh->db, query, -1, &dbh->stmt, NULL)) != SQLITE_OK )
    {
            logger("Funcion %s (%d): executing: <%s> \n- Error <%d>", __FUNCTION__, __LINE__, query, res);
            free(column_names);
            return RPC_FAIL;
    }

    
    printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
    ncolumn = sqlite3_column_count(dbh->stmt);
    nrow = 1;
    for(i=0;i<ncolumn;i++)
    {
        list_add_str( column_names, i+1, sqlite3_column_name(dbh->stmt,i));
    }

    list_add_int( lb_reply, 1, ncolumn ); 
    list_add_int( lb_reply, 2, nrow);		
    list_add_raw( lb_reply, 3, column_names, length_list(column_names) ); 
    free(column_names);
    return RPC_OK;
    
}



e_rpc_t db_get_next_row(LIST_t* lb_rcv, LIST_t* lb_reply)
{
    handler *dbh;
    unsigned int ncolumn,i;
    char *value;
    LIST_t *column_values = list_alloc(20480);

    dbh = (handler *)list_get_ptr(lb_rcv,1);	

    if( sqlite3_step(dbh->stmt) == SQLITE_ROW )
    {
        // int get_row_count(handler *db_con);
        // int *get_column_sizes(handler *db_con);
        printf ("Funcion %s (%d): hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
        ncolumn = sqlite3_column_count(dbh->stmt);
        for(i=0;i<ncolumn;i++)
        {
            value = (char *)sqlite3_column_text(dbh->stmt,i);
            if(value)
            {
                logger("%5i:: %s",i, value );
                list_add_str( column_values, i+1,value);
            }
        }
        list_add_raw( lb_reply, 1, column_values, length_list(column_values) ); 
        free(column_values);
        return RPC_OK;  
    }
    printf ("Funcion %s (%d): no hay datos\n", __FUNCTION__, __LINE__);fflush(stdout);
    free(column_values);
    return RPC_FAIL;
}



void do_exit()
{
    puts("chau mundo");
}

void registrar_funciones(void)
{
    registrar_exit_funcion(do_exit);
    registrar_funcion(funcion_test); 
    registrar_funcion(w_fopen); 
    registrar_funcion(w_fread); 
    registrar_funcion(db_connect); 
    registrar_funcion(db_disconnect); 
    registrar_funcion(db_get_rows); 
    registrar_funcion(db_get_next_row);  
}


unsigned int clen = sizeof(struct sockaddr_in);
struct sockaddr_in client;
    
int main() 
{    
    int sock,csock;
    
    // sqlite3_stmt *result; 
    
    logger("Init");
    
    // sock = bind_tcp("10.210.77.77",7575,10);   
    sock = bind_tcp("0.0.0.0",7576,10);    
    printf("sock:%i\n",sock); 
    if(sock<0)
        return 0;
    
    registrar_funciones();
    
    while(1)
    {
        csock = accept(sock, (struct sockaddr *)&client, &clen);
        logger("[RPCD]Conexion desde: %s:%d",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
        int pid=fork();
        if(pid==0) 
        {
            loop_parser_query( csock,csock ); 
        } 
        close(csock);
        logger("[RPCD]Conexion pid:%i %s:%d ",pid,inet_ntoa(client.sin_addr),ntohs(client.sin_port)); 
    } 
    
    return 0;
}
