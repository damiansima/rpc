/* 
 * File:   rpc_test.h
 * Author: sima
 *
 * Created on 21 de septiembre de 2014, 21:00
 */

#ifndef RPC_TEST_H
#define	RPC_TEST_H

#include "lista_continua.h"
#include "rpc_client.h"

#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <sys/un.h>
#include <string.h>



void funcion_test(RPC_t *rpc);

int w_fread(RPC_t *rpc, char *data, int size, FILE *fp);

FILE *w_fopen(RPC_t *rpc, char *filename, char *mode);

#endif	/* RPC_TEST_H */

                             
