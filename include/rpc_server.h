/* 
 * File:   rpc_test.h
 * Author: sima
 *
 * Created on 21 de septiembre de 2014, 21:00
 */

#ifndef RPC_SERVER_H
  #define	RPC_SERVER_H  

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 

#include "rpc_common.h"

typedef e_rpc_t (*__function) (LIST_t *, LIST_t *);   
typedef void (*__on_exit) (void);    

typedef struct
{
    char name[128];  
    __function launch;          
}rpc_funcion;


#define registrar_funcion( f ) __registrar_funcion( #f , f )  
int __registrar_funcion(char *function_name,__function f);

e_rpc_t do_function(char *function_name, void *data_rcv, void *data_rply );

void loop_parser_query( int fd_write, int fd_read );

void registrar_exit_funcion(__on_exit f);

#endif
