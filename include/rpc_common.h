#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <signal.h>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>                          
#include <sys/select.h>
#include <sys/un.h>
#include <string.h>

#include "lista_continua.h"

#ifndef __RPC_COMMON_
    #define __RPC_COMMON_

#ifndef SUCCESS
    #define SUCCESS 0
#endif

#ifndef FAIL
    #define FAIL -1
#endif

typedef enum{
    RPC_OK=0,
    RPC_FAIL=-1, 
    RPC_FAIL_CONNECTION=-2, 
    RPC_FAIL_NO_FUNCTION=-3,
    RPC_NO_CONNECTED=-4 
}e_rpc_t; 
    

typedef struct{ 
    struct {
        char function[128];
        unsigned int length;
    }header;
    char data[10240];
}rpc_query;

typedef struct{ 
    struct {
        int return_value;
        unsigned int length;
    }header;
    char data[10240];  
}rpc_reply;


#ifndef stricmp
	#define stricmp(a,b) strcasecmp(a,b) 
#endif


char *rpc_strerror(e_rpc_t error);

int list_add_raw( LIST_t *list, int id, void *value, int size);
int list_add_float(LIST_t *list, int id, float value);
int list_add_int(LIST_t *list, int id, long int value);
#define list_add_str(list,id,value) list_add_raw(list,id,(char *)value,strlen(value)+1)
#define list_add_ptr(list,id,ptr) list_add_int( list, id, (int)((void *)ptr))

#define list_has_id(list,id) ( list_get_block(list,id)!=NULL ? 1 : 0 )

#define list_get_int(list,id) (*((long int *)((list_get_block(list,id))->data)))
#define list_get_float(list,id) (*((float *)((list_get_block(list,id))->data)))
#define list_get_str(list,id) ((char *)((list_get_block(list,id))->data))    
#define list_get_raw(list,id) ((void *)((list_get_block(list,id))->data))   
#define list_get_ptr(list,id) ((void *)(*((long int *)((list_get_block(list,id))->data))))


/**
 * int create_tcp_socket(char *ip, int port)
 *      Define un socket y se conecta con la ip y puerto dado.
 *      tiene un tiempo de 100mseg para abortar si no se puede conectar.
 * 
 * @return: sock abierto si se conecta bien, 
 *          -1 si ocurrio una error.
 */
int create_tcp_socket(char *ip, int port);

/**
 * int sendall(int sock,char *buf,int len)
 * 
 * Bucle para enviar, sale cuando envia todo o algun error.
 * @return: 0 si envia todo 
 */
int sendall(int sock, char *buf, int len); 


/**
 * int readall( int sock, char *buf, int len,int timeseg )
 * 
 * Lee hasta la longitud len, solo termina cuando lee todo o si se cumple el 
 * tiempo de timedout  
 *  
 * @param sock: socket TCP
 * @param buf: buffer donde guardar los datos leidos
 * @param len: longitud a leer
 * @param timeseg: timedout en segundos para empezar a leer     
 * @return: retorna len si pudo leer completamente 
 *          0 si sale por timedout 
 *          -1 si hay algun error   
 */
int readall(int sock, char *buf, int len, int timeseg);


/**
 * int bind_tcp(char *ip, int port, int liste)
 *  
 * @param ip: un string de direccion ip
 * @param port: puerto a usar
 * @param liste: numero de connecciones para listen
 * @return: retorna un socket abierto y configurado si sale todo ok,
 *          -1 si hay algun error
 */
int bind_tcp(char *ip, int port, int liste);


#endif
