#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 


#ifndef __RPC_CLIENT
    #define __RPC_CLIENT

#include "rpc_common.h" 

typedef struct {
    int sock;
    char ip[48];
    int port;
    char ip_sec[48];  
    int port_sec;
    int timeout;
}RPC_t;

/**
 * void rpc_free( RPC_t *rpc )
 *      Se desconecta y libera la memoria allocada.
 */
void rpc_free( RPC_t *rpc );

/**
 * RPC_t *rpc_init( char *file_cfg, char *seccion)
 *      Inicializa el puntero con el archivo de configuracion, leyendo la 
 *      seccion deseada.
 * 
 * @return: retorna el puntero inicializado si todo resulto correctamente
 *          NULL si ocurrio una error.
 */
RPC_t *rpc_init( char *file_cfg, char *seccion);

/**
 * e_rpc_t rpc_connect(RPC_t *rpc)
 *      se conecta con el servidor
 * 
 * @return: RPC_OK si se conecta o RPC_NO_CONNECTED 
 */
e_rpc_t rpc_connect(RPC_t *rpc);

/**
 * e_rpc_t rpc_send_msg( RPC_t *rpc, char *call_function, LIST_t *lista_send,LIST_t **lista_reply )
 *      Se usa para enviar el mensaje al servidor, llamando a la funcion en el servidor
 *      le envia la lista lista_send y recibe la respuesta en lista_reply.
 * 
 * @return: RPC_OK (según el return de la funcion en servidor) o retrna el codigo de 
 *      error segun corresponda e_rpc_t
 */
e_rpc_t rpc_send_msg( RPC_t *rpc, char *call_function, LIST_t *lista_send,LIST_t **lista_reply );

/* 
 * Funciones Privadas
 */
int read_cfg_file(RPC_t *rpc, char *file_cfg, char *COD_SECC) ;
#define rpc_is_connect(rpc) (((RPC_t*)rpc)->sock > 0 ? 1 : 0 )
#endif
